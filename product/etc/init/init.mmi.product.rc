# common initialization
on post-fs
    # LMK
    setprop ro.lmk.file_low_percentage 20
    setprop ro.lmk.pgscan_limit 5000
    setprop ro.lmk.swap_free_low_percentage 10
    setprop ro.lmk.file_high_percentage 70
    setprop ro.lmk.swap_util_max 90
    setprop ro.lmk.thrashing_limit 50
    setprop ro.lmk.min_thrashing_limit 10
    setprop ro.lmk.thrashing_limit_decay 25
    setprop ro.lmk.thrashing_limit_critical 50
    setprop ro.lmk.threshold_decay 50
    setprop ro.lmk.filecache_min_kb 300000
    # LMK 3.0
    setprop ro.lmk.use_moto_strategy true
    setprop ro.lmk.kswapd_limit 90
    setprop ro.lmk.kswapd_limit_decay 20
    setprop ro.lmk.freeze_min_adj 0
    setprop ro.lmk.stall_limit_freeze 8
    setprop ro.lmk.critical_min_adj 201
    setprop ro.lmk.stall_limit_critical 6
    setprop ro.lmk.medium_min_adj 920
    setprop ro.lmk.psi_partial_stall_ms 50
    setprop ro.lmk.psi_complete_stall_ms 150
    setprop ro.lmk.kill_heaviest_task true
    setprop ro.lmk.kill_timeout_ms 100
    setprop ro.lmk.camera_boost 0
    setprop ro.lmk.camera_boost_critical 2
    setprop ro.lmk.app_launch_boost 0
    setprop ro.lmk.scroll_boost 0
    setprop persist.lmk.debug true
    # App compactor
    setprop ro.config.use_compaction true
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_bootcompleted true
    setprop ro.config.compact_post_boot true
    # Zram
    setprop ro.vendor.zram.product_swapon true
    setprop ro.zram.mark_idle_delay_mins 60
    setprop ro.zram.first_wb_delay_mins 1440
    setprop ro.zram.periodic_wb_delay_hours 24
    write /sys/block/zram0/comp_algorithm lz4
    write /proc/sys/vm/swappiness 100
    setprop sys.sysctl.swappiness 100
    # LowMemoryDetector of Framework
    setprop ro.lowmemdetector.psi_low_stall_us 50000
    setprop ro.lowmemdetector.psi_medium_stall_us 150000
    setprop ro.lowmemdetector.psi_high_stall_us 200000
    # Promote recent UI procs
    setprop persist.sys.aitune_promote_recent_ui_procs true
    setprop persist.sys.aitune_promote_important_apps true
    # Disable Process pool
    setprop persist.device_config.runtime_native.usap_pool_enabled false
    # Enable freezer
    setprop ro.config.use_freezer true
    setprop ro.config.freezer_debounce_timeout 60000
    # moto: enable MTK performance framework
    setprop persist.sys.perf_fwk_enabled true
    setprop persist.sys.allow_aosp_hints true
    # enable /system/bin/init.mmi.system_tuning.sh
    setprop persist.sys.mmi_system_tuning true

    setprop ro.config.no_kill_duration_post_boot 0
    # Dalvik configuration
    setprop dalvik.vm.dex2oat-threads 4
    setprop dalvik.vm.dex2oat-cpu-set 2,3,4,5

    # do not pin dex files
    setprop ro.config.donot_pin_dex true
    # Cpuset for boost
    write /dev/cpuset/boost-app/cpus 1-7
    setprop debug.sf.boost_sf_on_touch true
    #Disable MTK INTERACTION boost, use moto boost instead
    setprop persist.sys.moto_boost_enabled true
    # moto app compact strategy
    setprop persist.sys.moto_cache_strategy true
    #dex2pro
    setprop persist.sys.dex2pro_enabled true
    setprop persist.sys.dex2pro_art_version 331813010

    #OverScroller
    setprop ro.config.use_moto_scroller true

    # max starting in bg, can be 1 in low ram device.
    setprop ro.config.max_starting_bg 8
    # use psi avg10 for mempressure in fwk to avoid ping-pong.
    setprop ro.config.use_psi_avg10_for_mempressure true
    # delay longer for service restart, will be rescheduled immediately once mempressure backing to normal.
    setprop ro.config.svc_restart_delay_on_moderate_mem 3600000
    setprop ro.config.svc_restart_delay_on_low_mem 3600000
    setprop ro.config.svc_restart_delay_on_critical_mem 3600000

#Reinit lmkd to reconfigure lmkd propertise
on property:sys.boot_completed=1
    setprop lmkd.reinit 1

on property:sys.boot_completed=1
    write /dev/cpuset/top-app/cpus 0-7
    # Cpuset for foreground apps
    # do not change to 0-5, CTS failed on IKSWT-32275
    write /dev/cpuset/foreground/cpus 0-6
    write /dev/cpuset/background/cpus 0-1
    write /dev/cpuset/restricted/cpus 0-3
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor sugov_ext
    write /sys/devices/system/cpu/cpu6/cpufreq/scaling_governor sugov_ext

on early-init
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor performance
    write /sys/devices/system/cpu/cpu6/cpufreq/scaling_governor performance

on property:sys.boot_completed=1 && property:ro.vendor.zram.product_swapon=true
    trigger sys-boot-completed-set

on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=false
    swapon_all /vendor/etc/fstab.enablezram
on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=true
    swapon_all /vendor/etc/fstab.enablezramwb

# Enable Zram writeback for 4~12G RAM
on property:persist.sys.zram_wb_enabled=""
    setprop persist.sys.zram_wb_enabled false
# Set Zram writeback size for 4~12G RAM
on post-fs && property:ro.vendor.hw.ram=4GB && property:persist.sys.zram_wb_size=""
    setprop persist.sys.zram_wb_size 1024M
on post-fs && property:ro.vendor.hw.ram=6GB && property:persist.sys.zram_wb_size=""
    setprop persist.sys.zram_wb_size 1536M
on post-fs && property:ro.vendor.hw.ram=8GB && property:persist.sys.zram_wb_size="" && property:ro.product.is_prc=""
    setprop persist.sys.zram_wb_size 2048M
on post-fs && property:ro.vendor.hw.ram=8GB && property:persist.sys.zram_wb_size="" && property:ro.product.is_prc="true"
    setprop persist.sys.zram_wb_size 3072M
on post-fs && property:ro.vendor.hw.ram=12GB && property:persist.sys.zram_wb_size=""
    setprop persist.sys.zram_wb_size 3072M


on property:ro.product.cpu.abi=arm64-v8a
    setprop dalvik.vm.dex2oat64.enabled true

# Tune Max bg apps
on property:ro.vendor.hw.ram=4GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 24
on property:ro.vendor.hw.ram=6GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 40
on property:ro.vendor.hw.ram=8GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 48
on property:ro.vendor.hw.ram=12GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 60
on property:ro.vendor.hw.ram=4GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 48
on property:ro.vendor.hw.ram=6GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 60
on property:ro.vendor.hw.ram=8GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 80
on property:ro.vendor.hw.ram=12GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 100

on  post-fs && property:ro.vendor.hw.ram=4GB
    setprop dalvik.vm.heapstartsize 8m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.75
    setprop dalvik.vm.heapminfree 512k
    setprop dalvik.vm.heapmaxfree 8m
    setprop ro.config.compact_action_1 2
    setprop ro.lmk.stall_limit_medium 0
on  post-fs && property:ro.vendor.hw.ram=6GB
    setprop dalvik.vm.heapstartsize 12m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 6m
    setprop dalvik.vm.heapmaxfree 24m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1
on  post-fs && property:ro.vendor.hw.ram=8GB
    setprop dalvik.vm.heapstartsize 16m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 8m
    setprop dalvik.vm.heapmaxfree 32m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1
on  post-fs && property:ro.vendor.hw.ram=12GB
    setprop dalvik.vm.heapstartsize 16m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 8m
    setprop dalvik.vm.heapmaxfree 32m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1

on property:ro.carrier="retin"
   setprop ro.setupwizard.require_network any
   setprop ro.setupwizard.user_req_network any

on boot
    #le scan interval & window downgrade
    setprop persist.mot_bt.le_scan_interval_downgrade true
