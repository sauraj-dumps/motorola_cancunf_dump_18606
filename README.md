## user 13 SQ1A.220105.002 2706d2 release-keys
- Manufacturer: motorola
- Platform: common
- Codename: cancunf
- Brand: motorola
- Flavor: user
- Release Version: 13
- Kernel Version: 5.10.168
- Id: SQ1A.220105.002
- Incremental: 2706d2
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: motorola/cancunf_g_hal/cancunf:12/SQ1A.220105.002/2706d2:user/release-keys
- OTA version: 
- Branch: user-13-SQ1A.220105.002-2706d2-release-keys
- Repo: motorola_cancunf_dump_18606
